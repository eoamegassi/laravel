<?php

return [
    'nom' => 'Nom',
    'prenom' => 'Prénom',
    'titremodification' => 'Modifier',
    'titreaffichage' => 'Affichage',
    'msgmiseajourok' => 'Etudiant mise à jour',
    'msgenregistrementok' => 'Etudiant ajoutée',
    'msgsupprimerok' => 'Etdiant supprimé',
];